<?php

namespace AppBundle\Admin;

use AppBundle\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class UserAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('usernameCanonical')
            ->add('email')
            ->add('emailCanonical')
            ->add('enabled')
            ->add('salt')
            ->add('password')
            ->add('lastLogin')
            ->add('confirmationToken')
            ->add('passwordRequestedAt')
            ->add('roles')
            ->add('id')
            ->add('name')
            ->add('avatar');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('username')
            ->add('usernameCanonical')
            ->add('email')
            ->add('emailCanonical')
            ->add('enabled')
            ->add('salt')
            ->add('password')
            ->add('lastLogin')
            ->add('confirmationToken')
            ->add('passwordRequestedAt')
            ->add('roles')
            ->add('id')
            ->add('name')
            ->add('avatar')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }


    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('username')
            ->add('email')
            ->add('password')
            ->add('avatar', null, array('template' => 'AppBundle:Admin:list_image.html.twig'));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class)
            ->add('username', TextType::class)
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
//            ->add('enaibled')
            ->add('imageFile', FileType::class);
    }



//    /**
//     * @param User $object
//     */
//
//    public function preUpdate($object)
//    {
//        $uniqid = $this->getRequest()->query->get('uniqid');
//        $formData = $this->getRequest()->query->get($uniqid);
//
//        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
//        if (array_key_exists('password', $formData)
//            && $formData['password'] !== null
//            && strlen($formData['password']) > 0) {
//            $object->setPassword($encoder->encodePassword($formData['password'], 123));
//        }
//    }
//
//    /**
//     * @param User $object
//     */
//
//    public function prePersist($object)
//    {
//        $uniqid = $this->getRequest()->query->get('uniqid');
//        $formData = $this->getRequest()->query->get($uniqid);
//
//        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
//        if (array_key_exists('password', $formData)
//            && $formData['password'] !== null
//            && strlen($formData['password']) > 0) {
//            $object->setPassword($encoder->encodePassword($formData['password'], 123));
//        }
//    }

}
