<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comments;
use AppBundle\Entity\Images;
use AppBundle\Entity\Likes;
use AppBundle\Entity\Subscriptions;
use AppBundle\Entity\User;
use AppBundle\Form\CommentType;
use AppBundle\Form\ImagesType;
use AppBundle\Form\likesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }


    /**
     * @Route("/home")
     */

    public function homeAction()
    {
        return $this->render('FOSUserBundle:Profile:show.html.twig', array());
    }

    /**
     * @Route("/edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function editAction(Request $request)
    {
        $images = new Images();

        $form = $this->createForm(ImagesType::class, $images);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();

            // $file - объект класса для управления скачанным файлом
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $images->getAvatar();

            // Генерируем уникальное имя файла, прежде чем сохраним его в папку с остальными файлами
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            // Перемещаем файл в папку с изображениями всех продуктов.
            $file->move(
                $this->getParameter('image_directory'),
                $fileName
            );
            // Сохраняем итоговое имя файла в поле сущности
            $images->setAvatar($fileName);
            $images->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($images);
            $em->flush();

            return $this->redirectToRoute("app_default_edit", [
                "id" => $images->getId()
            ]);
        }

        $user = $this->getUser()->getUsername();
        $avatar = $this->getUser()->getAvatar();

        return $this->render('FOSUserBundle:Profile:edit.html.twig', array(
            'user' => $user,
            'avatar' => $avatar,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/images")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function imagesAction(Request $request)
    {
        $images = $this->getDoctrine()
            ->getRepository('AppBundle:Images')
            ->findAll();
        $comments = $this->getDoctrine()
            ->getRepository('AppBundle:Comments')
            ->findAll();

        return $this->render(':default:images.html.twig', array(
            'images' => $images,
            'comments' => $comments
        ));
    }

    /**
     * Creates a new likes entity.
     * @Route("/like/{id}", requirements={"id": "\d+"})
     * @Method({"GET","POST"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function likeAction(Request $request, int $id)
    {
        $likes = new Likes();
        $em = $this->getDoctrine()->getManager();
        $likes->setUser($this->getUser());
        $likes->setImages($this->getDoctrine()
            ->getRepository('AppBundle:Images')
            ->find($id));
        $likes->setLike(1);
        $em->persist($likes);
        $em->flush();
        return $this->redirectToRoute('app_default_images');
    }

    /**
     * Creates a new Subscriptions entity.
     * @Route("/Subscription/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function subscriptionAction(Request $request, int $id)
    {
        $subscriptions = new Subscriptions();
        $em = $this->getDoctrine()->getManager();
        $subscriptions->setUser($this->getUser());
        $subscriptions->setImages($this->getDoctrine()
            ->getRepository('AppBundle:Images')
            ->find($id));
        $subscriptions->setSubscription(1);
        $em->persist($subscriptions);
        $em->flush();
        return $this->redirectToRoute('app_default_images');
    }


    /**
     * Creates a new Comments entity.
     * @Route("/Comments/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function commentsAction(Request $request, int $id)
    {
        $comments = new Comments();
        $em = $this->getDoctrine()->getManager();
        $comments->setUser($this->getUser());
        $comments->setImage($this->getDoctrine()
            ->getRepository('AppBundle:Images')
            ->find($id));
        $comments->setComments($_REQUEST['comment']);
        $em->persist($comments);
        $em->flush();
        return $this->redirectToRoute('app_default_images');
    }
}
