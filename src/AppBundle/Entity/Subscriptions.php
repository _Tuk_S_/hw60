<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subscriptions
 *
 * @ORM\Table(name="subscriptions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubscriptionsRepository")
 */
class Subscriptions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="subscription", type="boolean")
     */
    private $subscription;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="Subscriptions")
     */

    private $user;

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }



    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Images", inversedBy="Subscriptions")
     */

    private $images;


    public function setImages(Images $images)
    {
        $this->images = $images;
    }

    public function getImages()
    {
        return $this->images;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subscription
     *
     * @param boolean $subscription
     *
     * @return Subscriptions
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Get subscription
     *
     * @return bool
     */
    public function getSubscription()
    {
        return $this->subscription;
    }
}

