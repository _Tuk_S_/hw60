<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Images
 *
 * @ORM\Table(name="images")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImagesRepository")
 */
class Images
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="User",
     *     inversedBy="articles"
     * )
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Images
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @ORM\Column(name="avatar", type="string")
     *
     * @Assert\NotBlank(message="Пожалуйста, укажите изображение продукта, иначе его никто не купит, зуб даю!")
     * @Assert\File(
     *     mimeTypes = { "image/gif", "image/jpeg", "image/png" },
     *     mimeTypesMessage = "Недопустимый тип данных ({{ type }}). Допустимы: {{ types }}."
     * )
     * @Assert\Image(
     *     minWidth = 200,
     *     minWidthMessage = "Слижком маленькая ширина изображения ({{ width }}px).
     * Минимальная ширина: {{ min_width }}px.",
     *     maxWidth = 400,
     *     maxWidthMessage = "Слишком широкое изображение ({{ width }}px). Максимальная ширина: {{ max_width }}px.",
     *     minHeight = 200,
     *     minHeightMessage = "Слишком маленькая высота изображения ({{ height }}px).
     * Минимальная высота: {{ min_height }}px.",
     *     maxHeight = 400,
     *     maxHeightMessage = "Слишком высокое изображение ({{ height }}px). Максимальная высота: {{ max_height }}px."
     * )
     */
    private $avatar;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return string
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }
    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return string
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Likes", mappedBy="images")
     */
    private $likes;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Likes", mappedBy="images")
     */
    private $subscriptions;


    public function __construct()
    {
        $this->subscriptions = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getLikes()
    {
        return $this->likes;
    }


    public function getSubscriptions()
    {
        return $this->likes;
    }

    /**
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Comments",
     *     mappedBy="image"
     * )
     */
    private $comments;


    /**
     * Add comments
     *
     * @param \AppBundle\Entity\Comments $comments
     *
     * @return Images
     */
    public function addComments(\AppBundle\Entity\Comments $comments)
    {
        $this->comments[] = $comments;
        return $this;
    }


    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }


}
