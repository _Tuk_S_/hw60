<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Likes
 *
 * @ORM\Table(name="likes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LikesRepository")
 */


class Likes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="like", type="boolean")
     */
    private $like;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="Likes")
     */

    private $user;


    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Images", inversedBy="Likes")
     */

    private $images;


    public function setImages(Images $images)
    {
        $this->images = $images;
    }

    public function getImages()
    {
        return $this->images;
    }



    /**
     * Set like
     *
     * @param boolean $like
     *
     * @return Likes
     */
    public function setLike($like)
    {
        $this->like = $like;
        return $this;
    }

    /**
     * Get like
     *
     * @return bool
     */
    public function getLike()
    {
        return $this->like;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

